Calculate the natural logarithm of a number, without using the default math library.  
This program calculates the natural logarithm using the ArcTanH of a number.